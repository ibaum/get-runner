# frozen_string_literal: true

require_relative "lib/get/runner/version"

Gem::Specification.new do |spec|
  spec.name = "get-runner"
  spec.version = Get::Runner::VERSION
  spec.authors = ["Ian Baum"]
  spec.email = ["ibaum@gitlab.com"]

  spec.summary = "Wrapper for deploying instances using GET"
  spec.description = spec.summary
  spec.homepage = "https://gitlab.com/ibaum/get-runner"
  spec.license = "MIT"
  spec.required_ruby_version = ">= 3.2.2"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = spec.homepage
  spec.metadata["rubygems_mfa_required"] = "true"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(__dir__) do
    `git ls-files -z`.split("\x0").reject do |f|
      (File.expand_path(f) == __FILE__) ||
        f.start_with?(*%w[bin/ test/ spec/ features/ .git .gitlab-ci.yml appveyor Gemfile])
    end
  end

  # TODO: files I want in there that aren't ready for GIT
  spec.files << "lib/get/runner/prechecks.rb"
  spec.bindir = "exe"
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency("thor")
  spec.add_dependency("toml-rb")
end
