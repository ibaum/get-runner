# frozen_string_literal: true

require "thor/group"

module Get
  module Runner
    class Prechecks < Thor::Group
      include Thor::Actions

      def self.exit_on_failure?
        false
      end

      def docker
        # We just need to check that this works. Not concerned about results yet
        run "docker ps > /dev/null", verbose: false
      rescue StandardError => e
        warn("There was an error connecting to Docker. Please make sure it is installed and running: #{e.inspect}")
        exit 1
      end

      def license
        # Check make sure it is in correct form
        results = run(
          "op --vault='#{parent_options[:vault]}' item list --tags get_runner_license_key --format json", capture: true, verbose: false
        )

        begin
          return unless JSON.parse(results).length != 1
        rescue JSON::ParserError
          nil
        end

        warn("I could not find the license in 1password")
        exit 1
      end
    end
  end
end
