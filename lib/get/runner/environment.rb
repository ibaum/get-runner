# frozen_string_literal: true

require "base64"
require "thor"

module Get
  module Runner
    class Environment < Thor
      include Thor::Actions

      SECRETS = %i[
        gcp_service_account_contents
        ssh_public_key
        ssh_private_key
        gitlab_license_text
        gitlab_root_password
        pgbouncer_password
        consul_password
        praefect_internal_token
        redis_password
        postgres_password
        gitlab_com_pat
      ].freeze

      def self.exit_on_failure?
        true
      end

      no_commands do
        def items(tag)
          JSON.parse(run("op item list --tags #{tag} --format json", capture: true, verbose: false))
        end

        def contents(item)
          run("op read op://#{parent_options[:vault]}/#{item}", capture: true, verbose: false).chomp
        end

        def find_or_create_password(title, tag, style="letters,digits,symbols,32")
          results = items(tag)

          return contents(%(#{results.first["id"]}/password)) unless results.empty?

          create_password(title, tag, style)
        end

        # TODO: this is failing, looks to be a disagreement with the way the thor action handles
        # stdin, and op's expectations about stdin.
        # https://github.com/1Password/op-vscode/issues/187
        def create_password(title, tags, style)
          create_cmd = "op item create --vault #{parent_options[:vault]} --category password " \
                       "--title \"#{title}\" --generate-password=\"#{style}\" " \
                       "--tags \"get_runner,#{tags}\" --format json"
          results = `#{create_cmd}`
          JSON.parse(results)["fields"].select { |field| field["id"].eql?("password") }.first["value"]
        end
      end

      desc "gcp_service_account_contents", "Fetch the contents of the service account key"
      def gcp_service_account_contents
        contents("#{items("get_runner_service_key").first["id"]}/key.json")
      end

      desc "ssh_public_key", "Fetch the contents of the SSH public key"
      def ssh_public_key
        contents(%("#{items("get_runner_ssh_key").first["id"]}/public key"))
      end

      desc "ssh_private_key", "Fetch the contents of the SSH private key"
      def ssh_private_key
        contents(%("#{items("get_runner_ssh_key").first["id"]}/private key?ssh-format=openssh"))
      end

      desc "gitlab_license_text", "Fetch the contents of the GitLab license"
      def gitlab_license_text
        contents(%("#{items("get_runner_license_key").first["id"]}/license key"))
      end

      # TODO: Find or create, or document adding this item to 1Password
      desc "gitlab_root_password", "Fetch the GitLab root password"
      def gitlab_root_password
        contents(%("#{items("get_runner_root_password").first["id"]}/password"))
      end

      desc "pgbouncer_password", "Fetch or create a pgbouncer password"
      def pgbouncer_password
        find_or_create_password("Get runner pgbouncer password", "get_runner_pgbouncer_password")
      end

      desc "praefect_internal_token", "Fetch or create a praefect_internal_token"
      def praefect_internal_token
        find_or_create_password("Get runner Praefect internal token", "get_runner_praefect_internal_token")
      end

      desc "gitaly_token", "Fetch or create a gitaly_token"
      def gitaly_token
        find_or_create_password("Get runner Gitaly token", "get_runner_gitaly_token")
      end

      desc "consul_password", "Fetch or create a consul password"
      def consul_password
        find_or_create_password("Get runner consul password", "get_runner_consul_password")
      end

      desc "redis_password", "Fetch or create a redis password"
      def redis_password
        find_or_create_password("Get runner redis password", "get_runner_redis_password", "letters,digits,32")
      end

      desc "postgres_password", "Fetch or create a postgres password"
      def postgres_password
        find_or_create_password("Get runner postgres password", "get_runner_postgres_password")
      end

      desc "gitlab_com_pat", "GitLab.com personal access token"
      def gitlab_com_pat
        item = items("get_runner_gitlab_com_pat").first
        contents(%("#{item["id"]}/password")) if item
      end

      method_option :env_file, type: :string # , required: true
      desc "create", "puts all of the information into an environment file"
      def create
        ::File.open(options[:env_file], "w") do |env_file|
          SECRETS.each do |secret|
            variable = "#{secret.upcase}_ENC"
            value = invoke(secret)
            next if value.nil?

            env_file.puts "#{variable}=#{Base64.strict_encode64(value)}"
          end
        end
      end
    end
  end
end
