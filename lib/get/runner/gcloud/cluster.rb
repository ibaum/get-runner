# frozen_string_literal: true

require "thor"

module Get
  module Runner
    class Gcloud < Thor
      class Cluster < Thor
        include Thor::Actions

        desc "credentials", "get credentials"
        def credentials
          clusters = run("gcloud container clusters list --format='get(name,location)'", capture: true, verbose: false)
          clusters.each_line do |cluster|
            name, location = cluster.split
            run("gcloud container clusters get-credentials #{name} --location #{location}")
          end
        end
      end
    end
  end
end
