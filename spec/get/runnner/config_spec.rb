# frozen_string_literal: true

RSpec.describe Get::Runner::Config do
  before do
    allow(TomlRB).to receive(:load_file).and_call_original
    allow(TomlRB).to receive(:load_file).with("config_file").and_return(config)
  end

  context "good config" do
    subject { described_class.new("config_file") }
    let(:config) do
      {
        "global" => {
          "architecture" => "1k",
          "cloud_provider" => "gcp"
        }
      }
    end

    let(:onek_gcp) do
      {
        "gitlab_rails_machine_type" => "n1-highcpu-8",
        "gitlab_rails_node_count" => 1,
        "haproxy_external_machine_type" => "n1-highcpu-8",
        "haproxy_external_node_count" => 1
      }
    end

    it "loads successfully" do
      expect(subject).to be_instance_of(described_class)
    end

    it "has an architecture with machines" do
      expect(subject.global["architecture"]["machines"]).to eq(onek_gcp)
    end
  end

  context "bad config" do
    subject { described_class.new("config_file") }
    let(:config) do
      {
        "global" => {
          "architecture" => "fakearch"
        }
      }
    end

    it "raises an error" do
      expect(subject).to be_instance_of(described_class)
    end
  end
end
