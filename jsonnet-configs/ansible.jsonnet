local configuration = std.parseJson(std.extVar('configuration'));
local geo_enabled = std.get(configuration.global, 'geo', true);
local plugin = 'gcp_compute';
local node_filter(site) = [
  'labels.gitlab_node_prefix = ' + configuration.environment.deployment_name + '-' + site,
];
local os_version = 'ubuntu-jammy';
local download_url(project, jobid) =
  if project != '' then
    'https://gitlab.com/api/v4/projects/' + project + '/jobs/' + jobid + '/artifacts/pkg/' + os_version + '/gitlab.deb'
  else
    ''
;

local package_repo = std.get(configuration.environment, 'repo', default=null);

local external_url(site) =
  if std.objectHas(configuration.global, 'dns_name') then
    'https://' + site + '.' + configuration.global.dns_name
  else
    ''
;

local region_from_zone(zone) =
  local components = std.split(zone, '-');
  std.join('-', [components[0], components[1]]);

local geo_primary_registry_url = 'https://registry.' + configuration.global.dns_name;

local cnh_custom_config =
  if std.get(configuration.architecture, 'cloud_native_hybrid', false) then
    {
      gitlab: {
        toolbox: {
          extraEnv: {
            CUSTOMER_PORTAL_URL: 'https://customers.staging.gitlab.com',
            GITLAB_LICENSE_MODE: 'test',
          },
        },
        webservice: {
          extraEnv: {
            CUSTOMER_PORTAL_URL: 'https://customers.staging.gitlab.com',
            GITLAB_LICENSE_MODE: 'test',
          },
        },
      },
    }
  else
    null
;

local cnh_var_geo(zone, role) =
  if geo_enabled then
    {
          cloud_native_hybrid_geo: true,
          cloud_native_hybrid_geo_role: role,
          geo_primary_site_prefix: configuration.environment.deployment_name + '-primary',
          geo_secondary_site_prefix: configuration.environment.deployment_name + '-secondary',
          geo_primary_site_gcp_project: configuration.environment.project,
          geo_secondary_site_gcp_project: configuration.environment.project,
          geo_primary_site_gcp_gke_location: region_from_zone(configuration.environment.primary_zone),
          geo_secondary_site_gcp_gke_location: region_from_zone(configuration.environment.secondary_zone),
    }
    else {}
    ;

local cnh_vars(zone, role) =
  if std.get(configuration.architecture, 'cloud_native_hybrid', false) then
    {
      cloud_native_hybrid_environment: true,
      cloud_native_hybrid_geo: false,
      kubeconfig_setup: true,
      gcp_gke_location: region_from_zone(zone),
      gitlab_charts_custom_config_file: '/srv/inventories/ansible/shared/files/gitlab_charts.yml.j2',
    } + cnh_var_geo(zone, role)
  else
    {}
;

local main_vars = {
  all: {
    vars: {
      gitaly_sharded_storage_path: "/mnt/gitlab/",
      cloud_provider: configuration.global.cloud_provider,
      container_registry_token: configuration.environment.crt,
      gcp_project: configuration.environment.project,
      external_ssl_source: 'letsencrypt',
      external_ssl_letsencrypt_issuer_email: configuration.global.letsencrypt_email,
      system_packages_auto_security_upgrade: false,
      service_account_file: '/tmp/creds.json',
      gitlab_root_password: "{{ lookup('env', 'GITLAB_ROOT_PASSWORD') }}",
      pgbouncer_password: "{{ lookup('env', 'PGBOUNCER_PASSWORD_ENC') | b64decode }}",
      consul_password: "{{ lookup('env', 'CONSUL_PASSWORD_ENC') | b64decode }}",
      gitaly_data_disks: [
        { device_name: 'data', mount_dir: '/var/opt/gitlab/git-data' },
      ],
      gitaly_token: "{{ lookup('env', 'GITALY_TOKEN_ENC') | b64decode }}",
      praefect_internal_token: "{{ lookup('env', 'PRAEFECT_INTERNAL_TOKEN_ENC') | b64decode }}",
      redis_password: "{{ lookup('env', 'REDIS_PASSWORD_ENC') | b64decode }}",
      postgres_password: "{{ lookup('env', 'POSTGRES_PASSWORD_ENC') | b64decode }}",
      gitlab_license_text: "{{ lookup('ansible.builtin.env', 'GITLAB_LICENSE_TEXT') }}",
      gitlab_rails_custom_config_file: '/srv/scripts/ansible/custom/gitlab_rails.rb.j2',
      gitlab_shell_ssh_daemon: 'gitlab-sshd',
    } + (
      if std.objectHas(configuration.environment, 'gitlab_version') then
        { gitlab_version: configuration.environment.gitlab_version }
      else if std.objectHas(configuration.environment, 'package_job_id') then
        { gitlab_deb_download_url: download_url(std.get(configuration.environment, 'package_project', ''), std.get(configuration.environment, 'package_job_id', '')) }
      else if package_repo == 'nightly' then
        { gitlab_repo_script_url: 'https://packages.gitlab.com/install/repositories/gitlab/nightly-builds/script.deb.sh' }
      else if std.objectHas(configuration.environment, 'package_download_url') then
        {
          gitlab_deb_download_url: configuration.environment.package_download_url,
          gitlab_deb_force_install: true,
        }
      else
        {}
    ),
  },
};

local url_site = 
    if geo_enabled then
      'primary'
    else
      'gitlab';

local primary_vars = main_vars {
  all+: {
    vars+: {
      prefix: configuration.environment.deployment_name + '-primary',
      external_url: external_url(url_site),
      geo_primary_external_url: external_url(url_site),
      geo_primary_site_group_name: 'geo_primary_site',
      geo_primary_site_name: 'Primary Site',
      container_registry_external_url: geo_primary_registry_url,
    } + (
      if std.objectHas(configuration.environment, 'geo_primary_internal_url') then
        { geo_primary_internal_url: configuration.environment.geo_primary_internal_url }
      else
        {}
    ) + cnh_vars(configuration.environment.primary_zone, 'primary'),
  },
};

local secondary_vars = 
if geo_enabled then 
  main_vars {
  all+: {
    vars+: {
      prefix: configuration.environment.deployment_name + '-secondary',
      external_url: external_url('secondary'),
      geo_secondary_external_url: external_url('secondary'),
      geo_secondary_site_group_name: 'geo_secondary_site',
      geo_secondary_site_name: 'Secondary Site',
      container_registry_external_url: 'https://registry.secondary.' + configuration.global.dns_name,
      geo_primary_registry_url: geo_primary_registry_url,
    } + (
      if std.objectHas(configuration.environment, 'geo_secondary_internal_url') then
        { geo_secondary_internal_url: configuration.environment.geo_secondary_internal_url }
      else
        {}
    ) + cnh_vars(configuration.environment.secondary_zone, 'secondary'),
  },
}
else
  null
;

local keyed_groups = [
  { key: 'labels.gitlab_node_type', separator: '' },
  { key: 'labels.gitlab_node_level', separator: '' },
  { key: 'labels.gitlab_geo_site', separator: '' },
  { key: 'labels.gitlab_geo_full_role', separator: '' },
];
local scopes = ['https://www.googleapis.com/auth/compute'];
local hostnames = ['name'];
local compose = { ansible_host: 'networkInterfaces[0].accessConfigs[0].natIP' };

local main_gcp = {
  auth_kind: 'serviceaccount',
  plugin: plugin,
  projects: [configuration.environment.project],
  keyed_groups: keyed_groups,
  scopes: scopes,
  hostnames: hostnames,
  compose: compose,
};

local secondary_main = 
  if geo_enabled then
    main_gcp { filters: node_filter('secondary') }
  else
    null; 

local outputFiles = [
  { fileName: 'primary/geo.gcp.yml', data: main_gcp { filters: node_filter('primary') } },
  { fileName: 'secondary/geo.gcp.yml', data: secondary_main },
  { fileName: 'primary/vars.yml', data: primary_vars },
  { fileName: 'secondary/vars.yml', data: secondary_vars },
  { fileName: 'shared/files/gitlab_charts.yml.j2', data: cnh_custom_config },
];

{ [entry.fileName]: std.manifestYamlDoc(entry.data, indent_array_in_object=true, quote_keys=false) for entry in outputFiles if entry.data != null }
