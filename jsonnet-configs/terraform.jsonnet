local configuration = std.parseJson(std.extVar('configuration'));
local region_from_zone(zone) =
  local components = std.split(zone, '-');
  std.join('-', [components[0], components[1]]);

local terraform_env = {
  deployment_name: configuration.environment.deployment_name,
  object_storage_buckets_force_destroy: true,
  project: configuration.environment.project,
};

local sa_prefix(prefix, site) =
  local words = [std.substr(i, 0, 1) for i in std.split(prefix, '-')];
  std.join('', words) + '-' + site;

local env_check_cnh(zone) =
  if !std.get(configuration.architecture, 'cloud_native_hybrid', false) then
    { haproxy_external_external_ips: ['${var.external_ip}'] }
  else
    { gke_location: zone }
;

local geo_enabled = std.get(configuration.global, 'geo', true);

local cloud_sql_backup =
  if std.get(configuration.environment, 'cloud_sql_backup_enabled', false) then
  {
    cloud_sql_postgres_backup_configuration: {
      enabled: true,
      retained_backups: 2,
      point_in_time_recovery_enabled: true,
      transaction_log_retention_days: 2,
    },
  }
  else
    {};

local cloud_sql_settings =
  if std.get(configuration.architecture.machines, 'cloud_sql_postgres_machine_tier', null) != null then
    {
      cloud_sql_postgres_root_password: '${var.postgres_password}'
    }
    else
    {};

local terraform_environment(zone) = {
  module: {
    gitlab_ref_arch_gcp: {
      // terraform treats relative paths differently than absolute paths, if we go with absolute we'll get the error
      // The given source directory for module.gitlab_ref_arch_gcp.module.redis_cache would be outside of its containing package....
      // Probably fixable, but not today
      source: '../../../../gitlab-environment-toolkit/terraform/modules/gitlab_ref_arch_gcp',
      prefix: '${var.prefix}',
      project: '${var.project}',
      service_account_prefix: '${var.sa_prefix}',
      machine_image: '${var.machine_image}',
      geo_site: '${var.geo_site}',
      gitaly_disks: [
        { device_name: "data", size: 500},
      ],
    } + configuration.architecture.machines + env_check_cnh(region_from_zone(zone)) + cloud_sql_backup + cloud_sql_settings,
  },
  variable: {
    external_ip: {},
    deployment_name: {},
    geo_site: {},
    machine_image: {},
    object_storage_buckets_force_destroy: {},
    postgres_password: {default: "BADPASSWORD"},
    prefix: {},
    project: {},
    region: {},
    //ssh_public_key_file: {},
    sa_prefix: {},
    zone: {},
  },
  output: {
    gitlab_ref_arch_gcp: {
      value: '${module.gitlab_ref_arch_gcp}',
    },
  },
};

local backend_config(state_prefix) = {
  terraform: {
    backend: {
      gcs: {
        bucket: configuration.environment.deployment_name + '-get-terraform-state',
        prefix: state_prefix,

      },
    },
  },
};

local provider_config = {
  provider: {
    google: {
      project: '${var.project}',
      region: '${var.region}',
      zone: '${var.zone}',
    },
  },
};

local site_main(state_prefix) = {
  resource: {
    google_compute_address: {
      external_ip: {
        name: '${var.prefix}-external',
        region: '${var.region}',
      },
    },
  },
  output: {
    external_ip_addr: {
      value: '${google_compute_address.external_ip.address}',
    },
  },
} + backend_config(state_prefix) + provider_config;

local shared_main = backend_config('shared') + provider_config;

local primary_terraform = {
  region: region_from_zone(configuration.environment.primary_zone),
  zone: configuration.environment.primary_zone,
  geo_site: 'geo-primary-site',
  machine_image: configuration.environment.machine_image,
  prefix: configuration.environment.deployment_name + '-primary',
  sa_prefix: sa_prefix(configuration.environment.deployment_name, 'pri'),
} + terraform_env;

local primary_main = site_main(primary_terraform.prefix);

local secondary_terraform = 
if geo_enabled then
{
  region: region_from_zone(configuration.environment.secondary_zone),
  zone: configuration.environment.secondary_zone,
  geo_site: 'geo-secondary-site',
  machine_image: configuration.environment.machine_image,
  prefix: configuration.environment.deployment_name + '-secondary',
  sa_prefix: sa_prefix(configuration.environment.deployment_name, 'sec'),
} + terraform_env
else
null;

local secondary_main = 
  if geo_enabled then
    site_main(secondary_terraform.prefix)
  else 
    null;

local tfArecord(name, rrdatas) = {
  resource: {
    google_dns_record_set: {
      [std.strReplace(name, '.', '_')]: {
          name: name + '.${data.google_dns_managed_zone.domainname.dns_name}',
          managed_zone: '${data.google_dns_managed_zone.domainname.name}',
          type: 'A',
          rrdatas: rrdatas,
      }
    }
  }
};

local aRecords = 
  if geo_enabled then
    [
      {
        name: 'primary',
        rrdatas: ['${resource.google_compute_address.primary_external.address}'],
      },
      {
        name: 'registry',
        rrdatas: ['${resource.google_compute_address.primary_external.address}'],
      },
      {
        name: 'registry.secondary',
        rrdatas: ['${resource.google_compute_address.secondary_external.address}'],
      },
      {
        name: 'secondary',
        rrdatas: ['${resource.google_compute_address.secondary_external.address}'],
      },
    ]
  else
    [
      {
        name: 'gitlab',
        rrdatas: ['${resource.google_compute_address.primary_external.address}'],
      },
      {
        name: 'registry',
        rrdatas: ['${resource.google_compute_address.primary_external.address}'],
      },
    ]
;

local base_shared_environment = [
  {
    data: {
      google_dns_managed_zone: {
        domainname: {
          name: '${var.domain}',
        },
      },
    },
  },
  {
    resource: {
      google_compute_address: {
        primary_external: {
          name: 'primary-external',
          region: '${var.primary_region}',
        },
      },
    },
  },
];

local shared_environment =
  base_shared_environment + (
    if geo_enabled then
    [
    {
      resource: {
        google_compute_address: {
          secondary_external: {
            name: 'secondary-external',
            region: '${var.secondary_region}',
          },
        },
      },
    }]
    else 
    []) + (
      [ tfArecord(record.name, record.rrdatas) for record in aRecords]
    );

local shared_variables_tf = {
  variable: {
    domain: {},
    primary_region: {},
    secondary_region: {},
    project: {},
    region: {},
    zone: {},
  },
};

local shared_output = {
  output: {
    primary_external_ip: {
      value: '${resource.google_compute_address.primary_external.address}',
    } 
  } + (
       if geo_enabled then
       {
        secondary_external_ip: {
          value: '${resource.google_compute_address.secondary_external.address}',
        }
       }
      else
        {}
  )
};

local shared_variables = {
  domain: configuration.global.domain,
  primary_region: region_from_zone(configuration.environment.primary_zone),
  secondary_region: region_from_zone(configuration.environment.secondary_zone),
  project: configuration.environment.project,
  region: region_from_zone(configuration.environment.primary_zone),
  zone: configuration.environment.primary_zone,
};

local primary_environment = terraform_environment(configuration.environment.primary_zone);
local secondary_environment =
 if geo_enabled then
  terraform_environment(configuration.environment.secondary_zone)
else
  null;

local outputFiles = [
  { fileName: 'primary/environment.tf.json', data: primary_environment },
  { fileName: 'primary/main.tf.json', data: primary_main },
  { fileName: 'primary/terraform.tfvars.json', data: primary_terraform },
  { fileName: 'shared/main.tf.json', data: shared_main },
  { fileName: 'shared/environment.tf.json', data: shared_environment },
  { fileName: 'shared/terraform.tfvars.json', data: shared_variables },
  { fileName: 'shared/variables.tf.json', data: shared_variables_tf },
  { fileName: 'shared/output.tf.json', data: shared_output },
  { fileName: 'secondary/terraform.tfvars.json', data: secondary_terraform },
  { fileName: 'secondary/environment.tf.json', data: secondary_environment },
  { fileName: 'secondary/main.tf.json', data: secondary_main },
  ];

{ [entry.fileName]: std.manifestJsonEx(entry.data, ' ') for entry in outputFiles if entry.data != null }
